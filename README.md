# Description
Embryon d'un jeu à la 1ere personne codé en C++.

# Objectifs
Sortir de la pièce en actionnant l'ouverture des portes.
Pour cela, il faut déposer suffisamment d'objets sur le déclencheur situé à l'endroit indiqué par une lumière verte, ou bien sortir avant que les portes ne se referment.

# Mouvements et actions du joueur
Se déplacer: touches ZQSD ou bien les flèches du clavier.
Regarder autour: mouvements de la souris.
Quitter le jeu: touche ESC suivi de clic gauche sur la croix ou clic gauche sur la fenêtre suivi des touches alt+F4

Sauter: touche espace.
Prendre un objet: être proche de l'objet et maintenir la touche Shift Gauche ou bouton Droit de la souris. Vous pouvez ensuite vous déplacer avec l'objet
Déposer un objet tenu: relâcher la touche Shift Gauche ou le bouton Droit de la souris. 

# Interactions
Le joueur peut porter et déplacer les objets.
Gestion des propriété physiques des objets (collisions, gravité)

# Copyrights
Tiré du tutoriel "Learn to Code in C++ by Developing Your First Game", disponible sur le site http: //www.udemy.com

------------------
(C) 2016 Steamlead (www.steamlead.com)