// (C) steamlead

using UnrealBuildTool;
using System.Collections.Generic;

public class UD03_BuildingEscapeEditorTarget : TargetRules
{
	public UD03_BuildingEscapeEditorTarget(TargetInfo Target)
	{
		Type = TargetType.Editor;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "UD03_BuildingEscape" } );
	}
}
