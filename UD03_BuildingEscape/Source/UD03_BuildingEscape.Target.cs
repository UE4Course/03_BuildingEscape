// (C) steamlead

using UnrealBuildTool;
using System.Collections.Generic;

public class UD03_BuildingEscapeTarget : TargetRules
{
	public UD03_BuildingEscapeTarget(TargetInfo Target)
	{
		Type = TargetType.Game;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "UD03_BuildingEscape" } );
	}
}
