// (C) steamlead

#include "UD03_BuildingEscape.h"
#include "OpenDoor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	Owner = GetOwner();
}

// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	if (PressurePlate == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("PressurePlate is not assigned"));
	}
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Poll trigger volume every frame
	if (GetTotalOfActorsOnPlate() > TriggerMass)
	{
		// open Door with broadcasting an event to blueprint
		OnOpen.Broadcast();
	}
	else
	{
		// Close Door with broadcasting an event to blueprint
		OnClose.Broadcast();
	}
}

float UOpenDoor::GetTotalOfActorsOnPlate()
{
	if (PressurePlate == nullptr)
	{
		return 0.0f;
	}

	float TotalMass = 0.0f;

	//Find all overlapping actors
	TArray<AActor*> OverlappingActors;
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);

	/// Iterate trought Actors to get their mass
	for (const auto& AnActor : OverlappingActors)
	{
		// get actor Mass
		/// Note : use getMass if simulate physics is activate on colisionComponent (but it creates drifting)
		/// TotalMass += MassComponent->GetMass();
		/// Note : use CalculateMass if simulate physics is activate on MeshComponent (it resolve drifting)
		UPrimitiveComponent* MassComponent = AnActor->FindComponentByClass<UPrimitiveComponent>();
		TotalMass += MassComponent->CalculateMass();
	}

	return TotalMass;
}
