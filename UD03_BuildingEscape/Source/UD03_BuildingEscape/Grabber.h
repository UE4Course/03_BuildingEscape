// (C) steamlead

#pragma once

#include "Components/ActorComponent.h"
#include "Grabber.generated.h"
#define OUT //Does nothing, just used to mark output parameters

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UD03_BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGrabber();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;

	// How far ahead from player can we reach in cm ?
	float Reach = 100.f;

	// Ray Cast and grab what we reach
	void Grab();

	// Release the actor grabbed
	void Release();

	// Find the (assumed) attached input component
	void FindPhysicHandleComponent();

	// Setup the (assumed) attached input component
	void SetupInputComponent();

	// Return hit for first physics body in reach
	const FHitResult GetFirstPhysicsBodyInReach();

	// Return current start of reach line
	FVector GetReachLineStart();

	// Return current end of reach line
	FVector GetReachLineEnd();
};
