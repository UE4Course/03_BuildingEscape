// (C) steamlead

#include "UD03_BuildingEscape.h"
#include "GrabIt.h"


// Sets default values for this component's properties
UGrabIt::UGrabIt()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	Owner = GetOwner();
}


// Called when the game starts
void UGrabIt::BeginPlay()
{
	Super::BeginPlay();
	if (PressurePlate == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("PressurePlate is not assigned"));
	}

	Player = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (Player == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Player or Pawn not found"));
	}
}

// Called every frame
void UGrabIt::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (PressurePlate == nullptr || Player==nullptr || Owner==nullptr)
	{
		return;
	}
	// Poll trigger volume every frame
	PlayerLocation = Player->GetActorLocation();

	// If the actor that grab is in the volume then we release the object
	if (PressurePlate->IsOverlappingActor(Player))
	{
		// is the actor grabbed ? 
		if (IsGrabbed)
		{
			// yes, we release it
			Release();
		}
	}

	// is the actor grabbed ? 
	if (IsGrabbed)
	{
		// yes, we move it with the grabber
		Owner->SetActorLocation(PlayerLocation + DiffLocation);
	}
	else
	{
		// No, we check if it can be grabbed 

		// get actor and actor that grab location
		ActorLocation = Owner->GetActorLocation();
		float ActorDistance = FVector::Dist(ActorLocation, PlayerLocation);
		//UE_LOG(LogTemp, Error, TEXT("ActorLocation:%f"), *ActorLocation.ToString());
		//UE_LOG(LogTemp, Error, TEXT("GrabberLocation:%f"), *PlayerLocation.ToString());
		//UE_LOG(LogTemp, Error, TEXT("Distance:%f"),ActorDistance);

		// is the actor that grab near the object ? 
		if (ActorDistance < 100.0f)
		{
			// yes, we grab it
			Grab();
		}
	}
}

void UGrabIt::Grab()
{
	if (Owner == nullptr || Player==nullptr )
	{
		return;
	}
	FVector NewLocation = Owner->GetActorLocation() + FVector(0.0f, 0.0f, -ZModifier);
	Owner->SetActorLocation(NewLocation);
	DiffLocation = ActorLocation - PlayerLocation;
	UE_LOG(LogTemp, Warning, TEXT("Grabbed By %s"), *Player->GetName());

	IsGrabbed = true;
}

void UGrabIt::Release()
{
	if (Owner == nullptr || Player == nullptr)
	{
		return;
	}
	FVector NewLocation = Owner->GetActorLocation() + FVector(0.0f, 0.0f, ZModifier);
	Owner->SetActorLocation(NewLocation);
	UE_LOG(LogTemp, Warning, TEXT("Released By %s"), *Player->GetName());

	IsGrabbed = false;
}
